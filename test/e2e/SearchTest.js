var config = require('../../nightwatch.conf.BASIC.js');

var pause = 5000;

module.exports = {
    'Launch the PropertyFinder UAE site': function (browser) {
        browser
            .resizeWindow(1280,800)
        	.url('http://www.propertyfinder.ae')
            .waitForElementVisible('body', 1000)
            .pause(1000)
    },

    'Enter request data to get the desired result': function (browser) {
        browser
        	.setValue('input[type=search]', 'Marina')
			.keys(['\uE015', '\uE006'])
			.click('#bedroom_group > div:nth-child(1) > div > button')
        	.click('#bedroom_group > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > ul:nth-child(1) > li:nth-child(4)')
        	.click('button[type=submit]')
        	.waitForElementVisible('body', 1000)
        	.pause(1000)
    },
	
    'Filter the result by price to get the lowest value result': function (browser) {
        browser
        	.click('div.ms-parent:nth-child(3) > button:nth-child(1)')
        	.click('div.ms-parent:nth-child(3) > div:nth-child(2) > ul:nth-child(1) > li:nth-child(3)')
        	.waitForElementVisible('body', 1000)
        	.pause(1000)
    },
    
    'Click the last element of the filtered result': function (browser) {
        browser
        	.click('#serp > ul > li:last-child > div.listing-content > h2 > a')
        	.waitForElementVisible('body', 1000)
        	.pause(1000)
    },

    'Check that the selection has two bathrooms': function (browser) {
     	browser
     		.assert.containsText(".fixed-table > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2)", "2")
     		.pause(1000)
     		.end();   
    }
};